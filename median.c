#include "headlines.h"
float median(float *tab, int elem)
{
    int i,j=0;
    int n;
    int swap_var;
    float median=0.0;
    for(i=0;i<elem-1;i++)
    {
        for(j=0;j<elem-i-1;j++)
        {
            if(*(tab+i)>*(tab+j+1))
            {
                swap_var=*(tab+j);
                *(tab+j)=*(tab+j+1);
                *(tab+j+1)=swap_var;
            }
        }
    }
    n=(n+1)/2-1;
    median=*(tab+n);
    return median;
}