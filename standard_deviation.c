#include "headlines.h"
float stand(float *tab,int elem)
{
    float sum=0.0, mean, SD=0.0;
    int i;
    for(i=0;i<elem;i++)
    {
        sum+=*(tab+i);
    }
    mean=sum/elem;
    for(i=0;i<elem;i++)
    {
        SD+=pow(*(tab+i)-mean,2);
    }
    return sqrt(SD/elem);
}

