#include <stdio.h>
#include <stdlib.h>
#include "headlines.h"
#define N 50
int main()
{
    float *x;
    float *y;
    float *RHC;
    x= (float*)malloc(N*sizeof(float));
    y= (float*)malloc(N*sizeof(float));
    RHC= (float*)malloc(N*sizeof(float));
    FILE *file;
    if((file=fopen("P0001_attr.txt","a+"))==NULL)
    printf("error");
    else
    {
        open(file,x,y,RHC);
        fprint("mean of x= %f",mean(x,N));
        fprint("median of x= %f",median(x,N));
        fprint("standard deviation of x= %f",SD(x,N));
        fprint("mean of y= %f",mean(y,N));
        fprint("median of y= %f",median(y,N));
        fprint("standard deviation of y= %f",SD(y,N));
        fprint("mean of RHC= %f",mean(RHC,N));
        fprint("median of RHC= %f",median(RHC,N));
        fprint("standard deviation of RHC= %f",SD(RHC,N));
    }
    flose(file);
    free(x);
    free(y);
    free(RHC);
    return 0;
}